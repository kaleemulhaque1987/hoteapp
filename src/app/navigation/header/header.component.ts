import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() toggleSideNavList =new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  toggleSideNav() {
    this.toggleSideNavList.emit();
    
  }

}
