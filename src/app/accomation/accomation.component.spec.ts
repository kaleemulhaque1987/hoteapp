import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccomationComponent } from './accomation.component';

describe('AccomationComponent', () => {
  let component: AccomationComponent;
  let fixture: ComponentFixture<AccomationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccomationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccomationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
