import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodBevarageComponent } from './food-bevarage.component';

describe('FoodBevarageComponent', () => {
  let component: FoodBevarageComponent;
  let fixture: ComponentFixture<FoodBevarageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodBevarageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodBevarageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
