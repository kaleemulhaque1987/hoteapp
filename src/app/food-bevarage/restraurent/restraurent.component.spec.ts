import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestraurentComponent } from './restraurent.component';

describe('RestraurentComponent', () => {
  let component: RestraurentComponent;
  let fixture: ComponentFixture<RestraurentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestraurentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestraurentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
