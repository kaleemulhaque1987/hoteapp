import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import { AccomationComponent } from './accomation/accomation.component';
import { FoodBevarageComponent } from './food-bevarage/food-bevarage.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
const routes:Routes =[
    {path:"accomdation",component:AccomationComponent},
    {path:"food-bevarage",component:FoodBevarageComponent},
    {path:"signin",component:SigninComponent},
    {path:"signup",component:SignupComponent},
    {path:"home",component:HomeComponent},
    {path:"",redirectTo:"/home",pathMatch:"full"}

]
@NgModule({
imports:[RouterModule.forRoot(routes)],
exports:[RouterModule]
})


export class AppRoutingModule {

}